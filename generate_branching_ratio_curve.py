import argparse
from os import listdir, system
from os.path import isfile, join
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import make_interp_spline, BSpline
from collections import defaultdict

matplotlib.rcParams['mathtext.fontset'] = 'cm'

def scrape_decays_and_branching_ratio_from_herwig_log(log_file_path):
    """Scrape decay strings and corresponding branching ratios from given path

    Args:
        log_file_path (string): File path of log file

    Returns:
        list: List of tuples containing the decay string and corresponding branching ratio
    """
    
    decays_and_branching_ratios = []
    with open(log_file_path, 'r') as log_file:
        # read the lines of the file
        lines = log_file.readlines()
        looping_through_decays = False
        # go through each line
        for line in lines:
            # if we come across a # then we have reached the end of the decays list
            if line.split() == ['#']:
                looping_through_decays = False
            # if still looping through list, split the line into decay name, BR, etc.
            if looping_through_decays:
                split_line = line.split()
                if split_line[0] != '#':
                    # append the decay name and BR to the main list
                    decays_and_branching_ratios.append((split_line[0][:-1], split_line[2]))
            # this string indicates the start of the decay list
            if '# Parent:' in line:
                looping_through_decays = True
                continue

    return decays_and_branching_ratios

parser = argparse.ArgumentParser(
                    prog='Branching ratio plotter',
                    description='Plots branching ratios against mass for specified parent particles or decays',
                    epilog='Text at the bottom of help')

parser.add_argument('filename')
parser.add_argument('-p', '--particle')
parser.add_argument('-P', '--parameter')
parser.add_argument('-m', '--mass_param_name')
parser.add_argument('-d', '--decays', nargs='*')
parser.add_argument('-c', '--child')
parser.add_argument('-T', '--threshold', type=float, default=0.0)
parser.add_argument('-n', '--name', default="my_plot")
args = parser.parse_args()

# first argument is the scan folder that contains all of the beam
# energies and individual parameter points (runpoints)
scan_folder = args.filename
parent_particle = args.particle
param_string = args.parameter
save_name = args.name
child_particle = args.child
user_decays = args.decays
mass_param_name = args.mass_param_name
br_threshold = args.threshold

print("User specified parent particle:", parent_particle)
if user_decays != None:
    print("User specified decays:", user_decays)

ALLOWED_BEAM_ENERGIES = ['13TeV', '2_76TeV', '7TeV', '8TeV']

# DATA STRUCTURE
# each row contains first the parameters in a dictionary
# then the list of tuples containing decays and BRs
# row = [{param_1: a, param_2: b, ...}, [(decay_1: br_1), (decay_2: br_2), ...]]
branching_ratio_data = []

print("Scraping Herwig log files for decays and branching ratios...")

# we want to select the decays that correspond to the user input param
# user-input param might not match actual params, so find the closest and use that
# TO-DO: maybe print the user param and then the closest found param?
closest_param_value = 0.0
param_name = str(param_string.split('=')[0])
param_value = float(param_string.split('=')[-1])

# get each runpoint folder for this energy
beam_folder = join(scan_folder, '13TeV')
runpoint_folders = [f for f in listdir(beam_folder) if not isfile(join(beam_folder, f))]
# iterate through each folder
for runpoint_folder in runpoint_folders:
    runpoint_folder = join(beam_folder, runpoint_folder)
    # get the files listed within this runpoint folder
    files = [f for f in listdir(runpoint_folder) if isfile(join(runpoint_folder, f))]
    # iterate through files in this runpoint folder
    for file in files:
        # if the folder contains a yoda file then it has successfully generated events
        if 'yoda' in file:
            # first get the params info
            param_dict = defaultdict(float)
            param_file_path = join(runpoint_folder, "params.dat")
            # scrape data from params.dat
            with open(param_file_path, 'r') as param_file:
                lines = param_file.readlines()
                for line in lines:
                    params = line.split()
                    param_dict[params[0]] = float(params[-1])
                    if abs(param_dict[param_name] - param_value) < abs(closest_param_value - param_value):
                        closest_param_value = param_dict[param_name]
            # now we know that this runpoint has a yoda, we have to loop again 
            # through files to find log file and scrape data
            decays_and_branching_ratios = []
            for file in files:
                # next scan through the log file for decays and branching ratios
                # check for both 'herwig' and 'log' because there are usually 
                # other non-herwig log files in the directory too
                if 'log' in file and 'herwig' in file:
                    file_path = join(runpoint_folder, file)
                    # scrape the BR data from one Herwig log file
                    decays_and_branching_ratios = scrape_decays_and_branching_ratio_from_herwig_log(file_path)
            # append a new row with the parameters and the decays and BR tuples
            branching_ratio_data.append([param_dict, decays_and_branching_ratios])
            break
                        
# there are lots of repeated decays (for each combination of params) so make set of decays
decays_set = set()
for params, brs in branching_ratio_data:
    for decay in brs:
        decays_set.add(decay[0])         
# create list corresponding to column names, including parameter names and decay names
# this will have the format (for n params, N unique decays):
# | param_1 | param_2 | ... | param_n | decay_1 | decay_1 | ... | decay_N |
columns = (list(params.keys()) + list(decays_set))
# create matrix to store each branching ratio entry
# each row will have all its param columns populated, but only one other
# column populated (that corresponding to the pertaining decay)
data = np.zeros(shape=(len(branching_ratio_data), len(columns)))
# insert BR data for each decay into the matrix
for i, (params, brs) in enumerate(branching_ratio_data):
    data[i,:len(params.keys())] = list(params.values())
    for decay in brs:
        data[i, columns.index(decay[0])] = decay[1]


# get only those decays that contain the user-specified particles
decays_to_plot = set()
for decay in decays_set:
    parent_and_child = decay.split('->')
    # case: child particle specified
    if child_particle != None and parent_particle in parent_and_child[0] and child_particle in parent_and_child[1]:
        decays_to_plot.add(decay)
    # case: child particle not specified
    elif child_particle == None and parent_particle in parent_and_child[0]:
        decays_to_plot.add(decay)
        
# if user has specified certain decays, check that they are in the decays containing parent
if user_decays != None:
    decays_to_plot = decays_to_plot.intersection(set(user_decays))

# plot branching ratios
print("Plotting branching ratios for the following decays:")          
plt.tight_layout()
fig, axs = plt.subplot_mosaic([['brs'], ['brs'], ['legend']], figsize=(6,5))
markers=['o','x','^','+', '*', 's', 'd']
# go through each decay we want to plot and find the BR corresponding to it
for i, decay_string in enumerate(decays_to_plot):
    # select rows that contain the param value closest to the user-specified value
    param_value_index = list(dict(param_dict).keys()).index(param_name)
    rows_same_param_value = (data[np.where(np.isclose(data[:,param_value_index], closest_param_value))])
    mass_value_index = list(dict(param_dict).keys()).index(mass_param_name)
    # select columns corresponding to the parent masses and the branching ratios of this decay
    masses_and_branching_ratios = rows_same_param_value[:,[mass_value_index, columns.index(decay_string)]]
    # separate masses and branching ratios
    masses = masses_and_branching_ratios[:,0]
    branching_ratios = masses_and_branching_ratios[:,1]
    # don't plot this decay if the branching ratio is below threshold
    if sum(branching_ratios) < br_threshold: continue
    print("\t{}".format(decay_string))
    axs['brs'].scatter(masses, branching_ratios, label=decay_string, marker=markers[i%len(markers)])
    
axs['legend'].legend(axs['brs'].get_legend_handles_labels()[0], axs['brs'].get_legend_handles_labels()[1], ncol=3, loc='center')    
axs['legend'].axis('off')
axs['brs'].set_xlabel(r"$M_{H} [GeV]$")
axs['brs'].set_ylabel(r"$BR(M_{H})$")
axs['brs'].set_yscale('log')
axs['brs'].grid()
axs['brs'].minorticks_on()
fig.savefig('{}.pdf'.format(save_name), bbox_inches='tight', transparent=True)